package com.atiruz;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComunaRepo extends JpaRepository<Comuna, Integer> {
}