package com.atiruz;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ComunaController {

	@Autowired
	private ComunaRepo comunaRepo;

	@GetMapping("/comuna")
	public List<Comuna> index() {
		return comunaRepo.findAll();
	}

	@GetMapping("/comuna/{id}")
	public Comuna show(@PathVariable String id) {
		int comunaId = Integer.parseInt(id);
		return comunaRepo.findOne(comunaId);
	}

	@RequestMapping(value = "/comunas", method = RequestMethod.GET)
	public Page<Comuna> findPaginated(@RequestParam(value = "page", defaultValue = "0") int page) {
		Page<Comuna> resultPage = comunaRepo.findAll(new PageRequest(page, 10));
		return resultPage;
	}
}
