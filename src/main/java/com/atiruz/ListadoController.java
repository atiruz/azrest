package com.atiruz;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ListadoController {
	@GetMapping("/")
	public String listado() {
		return "index";
	}
}
